# ProcessusDePositionnementFrontProject
This project is a web application for the positioning of consultants

contains two types of account with usernames and passwords

+ a Super-Admin account (Commercial Director) which allows you to:
    * add a new Admin
    * suspend an existing Admin
    * modify an admin

+ an Admin (Commercial) account can:
    * Managed all the steps related to the positioning process


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.0.

## Prerequisites
```
$ git clone https://Ahmed_Hssin@bitbucket.org/Ahmed_Hssin/processus-de-positionnement-front-project.git
$ npm i

```

+ Super-Admin account :
    * username = superAdmin123
    * password = superAdmin@pass

+ Admin account :
    * username = admin
    * password = admin

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Built With
- __[Angular](https://angular.io/)__ - Framework côté client
- __[Angular material](https://material.angular.io/)__ - Internationalized and accessible components for everyone.
- __[Angular notifications](https://www.npmjs.com/package/angular2-notifications)__ - A light and easy to use notifications library for Angular 2